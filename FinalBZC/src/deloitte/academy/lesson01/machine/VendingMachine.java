package deloitte.academy.lesson01.machine;
import java.util.logging.Logger;
import deloitte.academy.lesson01.entity.Inventory;
import deloitte.academy.lesson01.entity.Product;

	/**
	 *Class VendingMachine  contains the busness logic for the sale of a product and
	 *the total sales of our vending machine.
	 * @author bzamorano@externosdeloittemx.com.
	 * @since 2020-03-11.
	 * @version 1.0
	 */
public class VendingMachine {
	private Logger logger = Logger.getLogger(VendingMachine.class.getName());

	private Inventory inventory;
	private double salesTotal;

	/**
	 * Constructor VendingMachine initializes our total sales in 0.
	 * @param inventory it's an object from the Inventory class.
	 */
	public VendingMachine(Inventory inventory) {
		this.inventory = inventory;
		this.salesTotal = 0;
	}
	
	/**
	 * Method sellProduct uses the removeProduct from our Inventory class to remove 
	 * a product from the stock every time a sale is made.
	 * @param productCode
	 */
	public void sellProduct(String productCode) {
		try {
			Product productToSell = this.inventory.getProductByCode(productCode);
			this.inventory.removeProduct(productCode);
			salesTotal += productToSell.getPrice();
		} catch (Exception exception) {
			logger.severe(exception.getMessage());
		}
	}
/**
 * Method getSalesTotal will return the total of our sales.
 * @return salesTotal which represents the total of our sales.
 */
	public double getSalesTotal() {
		return this.salesTotal;
	}
}