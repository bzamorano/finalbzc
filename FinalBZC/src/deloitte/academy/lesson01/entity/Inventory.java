package deloitte.academy.lesson01.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class Inventory defines the quantities of our products in the vending.
 * machine. Some business logic it's injected in this class.
 * @since 2020-03-11.
 * @version 1.0
 * {@link Package java.util.HashMap https://docs.oracle.com/javase/7/docs/api/java/util/Map.html}
 * @category entity class
 * @implNote The method removeProduct will be implemented in the VendigMachine class.
 * @author bzamorano@externosdeloittemx.com.
 */
public class Inventory {
	

	/**products has the list of existing products. The value of this map is the object Product.
	 */
	
	private Map<String, Product> products;
	/**productQuantities keeps the quantities of these products. The value of this map the quantity of that product
	 */
	private Map<String, Integer> productQuantities;

	/**
	 * Constructor Inventory initialize two maps. We can use the productCode as the
	 * key for both maps.
	 */
	public Inventory() {
		this.products = new HashMap<String, Product>();
		this.productQuantities = new HashMap<String, Integer>();
	}

	/**
	 * One of the restrictions we have, is that we can only use a list of concrete
	 * codes. Method getValidCodes will obtain a list of these codes.
	 * 
	 * @return A list of Strings with the codes.
	 */
	public List<String> getValidCodes() {
		List<String> validCodes = new ArrayList<>();
		validCodes.add("A1");
		validCodes.add("A2");
		validCodes.add("A3");
		validCodes.add("A4");
		validCodes.add("A5");
		validCodes.add("A6");
		validCodes.add("B1");
		validCodes.add("B2");
		validCodes.add("B3");
		validCodes.add("B4");
		validCodes.add("B5");
		validCodes.add("B6");
		validCodes.add("C1");
		validCodes.add("C2");
		validCodes.add("C3");
		validCodes.add("C4");
		return validCodes;
	}

	/**
	 * Method isValidProductCode will validate the product's code. With this method,
	 * we can change a lower case code to an upper case. This ensures or code is
	 * valid, even if the input it's in lower case. For example(a1 to A1).
	 * 
	 * @param productCode Must be a String value.
	 * @return A string with the upper case code.
	 */

	public boolean isValidProductCode(String productCode) {
		String upperCasedProductCode = productCode.toUpperCase();
		List<String> validCodes = getValidCodes();
		return validCodes.contains(upperCasedProductCode);
	}

	/**
	 * Method addProduct will use the put method from the Map class. The
	 * responsabilitie of addProduct is to add X product to the inventary, and if
	 * this product alredy exists, it's going to update the amount of that product
	 * in the inventary.
	 * 
	 * @param product  object from the Product class.
	 * @param quantity is the quantity of the product.
	 * @throws Exception In case the Product's code it is not valid.
	 */

	public void addProduct(Product product, int quantity) throws Exception {
		String productCode = product.getCode().toUpperCase(); // We use product codes in upper case regardless of user input
																
		if (!isValidProductCode(productCode)) {
			throw new Exception(
					"The product code " + productCode + " is not valid, valid ones are: " + getValidCodes());
		}

	
	}

	/**
	 * Method removeProduct will remove a product considering the validations we
	 * have. In case the product is over, it will throw an exception.
	 * 
<<<<<<< master
=======
	 * @param productCode Must be a String value. This represents de product's code.
	 * @throws Exception in case the product's code it's on lower case.
	 */

	public void removeProduct(String productCode) throws Exception {
		String upperCasedProductCode = productCode.toUpperCase();
		if (!isValidProductCode(upperCasedProductCode)) {
			throw new Exception(
					"The product code " + upperCasedProductCode + " is not valid, valid ones are: " + getValidCodes());
		}

		if (!this.products.containsKey(upperCasedProductCode)
				|| !this.productQuantities.containsKey(upperCasedProductCode)) {
			throw new Exception(
					"Although the product key is valid, the product hasn't been registered in the inventory");
		}

		int currentProductQuantity = this.productQuantities.get(upperCasedProductCode);

>>>>>>> 6f260c9 hola borra
		// Out of stock
		if (currentProductQuantity == 0) {
			throw new Exception("Product on " + upperCasedProductCode + " is out of stock");
		}

		// Every validation passed
		this.productQuantities.put(upperCasedProductCode, currentProductQuantity - 1);
	}

	/**
	 * Since we can know a product's price depending of the product's code, we need
	 * a method to get the product's code. Method getProductByCode has this
	 * responsabilitie.
	 * 
	 * @param productCode Must be a String value.
	 * @return a Product object
	 * @throws Exception In case the product it is not available.
	 */

	public Product getProductByCode(String productCode) throws Exception {
		String upperCasedProductCode = productCode.toUpperCase();
		if (!isValidProductCode(upperCasedProductCode)) {
			throw new Exception(
					"The product code " + upperCasedProductCode + " is not valid, valid ones are: " + getValidCodes());
		}

		if (!this.products.containsKey(upperCasedProductCode)) {
			throw new Exception(
					"Although the product key is valid, the product hasn't been registered in the inventory");
		}

		Product product = this.products.get(upperCasedProductCode);

		if (product == null) {
			throw new Exception("The product is registered, but it holds a null value");
		}

		return product;
	}

}
