package deloitte.academy.lesson01.entity;

/**
 * Class Product defines an entity product. This product represents an object in
 * our vending machine.
 * @since 2020-03-11
 * @author bzamorano@externosdeloittemx.com.
 */
public class Product {

	private String code;
	private double price;
	private String name;

	public Product(String code, double price, String name) {
		super();
		this.code = code;
		this.price = price;
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;

	}

}
