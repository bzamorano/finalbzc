package deloitte.academy.lesson01.run;

import deloitte.academy.lesson01.entity.Inventory;
import deloitte.academy.lesson01.entity.Product;
import deloitte.academy.lesson01.machine.*;
/**
 * Class Run merges and executes our classes.
 * @author bzamorano@externosdeloittemx.com.
 * @since 2020-03-11.
 * @version 1.0
 * @exception On our class Run, an exception will be executed in the case an unvalid code it's put.
 * @see IOException on input error
 */
class Run {

    public static void main(String[] args) {
        Inventory inventory = new Inventory();

        /** Let's group all inventory set up.
         */
        try {
            inventory.addProduct(new Product("A1", 10.50, "Chocolate"), 10);
            inventory.addProduct(new Product("A2", 15.50, "Doritos"), 4);
            inventory.addProduct(new Product("A3", 22.50, "Coca"), 2);

            
            
            inventory.addProduct(new Product("A4", 8.75, "Gomitas"), 6);
            inventory.addProduct(new Product("A5", 30.00, "Chips"), 10);
            inventory.addProduct(new Product("A6", 15.00, "Jugo"), 2);
            inventory.addProduct(new Product("B1", 10.00, "Galletas"), 3);
            inventory.addProduct(new Product("B2", 120.00, "Canelitas"), 6);
            inventory.addProduct(new Product("B3", 10.10, "Halls"), 10);
            inventory.addProduct(new Product("B4", 3.14, "Tarta"), 10);
            inventory.addProduct(new Product("B5", 15.55, "Sabritas"), 0);
            inventory.addProduct(new Product("B6", 12.25, "Cheetos"), 4);
            inventory.addProduct(new Product("C1", 10.00, "Rocaleta"), 1);
            inventory.addProduct(new Product("C2", 14.75, "Rancherito"), 6);
            inventory.addProduct(new Product("C3", 13.15, "Rufles"), 10);
            inventory.addProduct(new Product("C4", 22.00, "Pizza Fria"), 9);
            inventory.removeProduct("A3");
            
        } catch (Exception exception) {
            System.out.println(exception.getMessage());
        }	

        /** Now that we have an inventory, we can create a VendingMachine
         */
        VendingMachine vendingMachine = new VendingMachine(inventory);

        
        try {                                // And now here we can make some sales
            vendingMachine.sellProduct("C1");
            vendingMachine.sellProduct("C2");
            vendingMachine.sellProduct("C2");
            //vendingMachine.sellProduct("Z7"); // And this last one should throw an exception because it's not stocked (Sabritas)
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        /**And then here we can print the total money earned on sales
         */
        System.out.println("Total from sales: $" + vendingMachine.getSalesTotal());
        System.out.println("Nuestros productos existentes son: " + inventory.getValidCodes());

    }

}

